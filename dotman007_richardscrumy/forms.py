from django.contrib.auth import models
from django.forms import ModelForm, fields
from .models import ScrumyGoals, GoalStatus, ScrumyHistory

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User, Group
from django import forms


class SignupForm(UserCreationForm):
    class Meta:
        model= User
        fields =['first_name', 'last_name', 'email', 'username', 'password1', 'password2' ]


class CreateGoalForm(ModelForm):
    class Meta:
        model= ScrumyGoals
        fields= ['goal_name',]

class MoveGoalForm(ModelForm):
    class Meta:
        model= ScrumyGoals
        fields=['goal_status',]

class GroupChange(forms.Form):
    group_name = forms.CharField(label='Group name', max_length=100)