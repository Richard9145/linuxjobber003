from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import SignupForm, CreateGoalForm, MoveGoalForm, GroupChange
from .models import ScrumyGoals, GoalStatus, ScrumyHistory
from django.contrib.auth.models import User, Group
import random
from django.contrib import messages

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required


# Create your views here.

# this is the registeration view
def index(request):
    form= SignupForm()
    
    if request.method =='POST':
        form= SignupForm(request.POST)
    
        if form.is_valid():
            user = form.save()

            group = Group.objects.get(name='Developer')
            user.groups.add(group)

            user= form.cleaned_data.get('username')
            messages.success(request, 'Account successfully created for '+ user)

            return redirect('dotman007_richardscrumy:login_page')
        else:
            form= SignupForm()

    context={'form': form}
    return render(request, 'dotman007_richardscrumy/index.html', context) 


# My Lab 21 task
def signUp(request):
    form = SignupForm()

    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            user=form.save()

            group = Group.objects.get(name='Developer')
            user.groups.add(group)

            return redirect('dotman007_richardscrumy:login_page')

    context={'form':form}
    return render(request, 'dotman007_richardscrumy/register.html', context)

# this is the loginPage view for lab 21
def loginPage(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('dotman007_richardscrumy:home')
        else:
            messages.info(request, 'Username OR Password is not Correct')

    context = {}
    return render(request, 'dotman007_richardscrumy/login_page.html', context)

def logoutUser(request):
    logout(request)
    return redirect('dotman007_richardscrumy:login_page')



def goal(request):
    goal = ScrumyGoals.objects.get(goal_name='Learn Django')

    return HttpResponse(goal)


def success(request):
    
    return render(request, 'dotman007_richardscrumy/success.html')


@login_required(login_url='dotman007_richardscrumy:login_page')
def move_goal(request, goal_id):

    try: 
        obj = ScrumyGoals.objects.get(goal_id=goal_id) 
    except Exception as e: 
        return render(
            request, 
            'dotman007_richardscrumy/exception.html', 
            {'error': 'A record with that goal id does not exist'}
        )
    else: 
        if request.method == 'POST':
            form = MoveGoalForm(request.POST)
            d_group = False
            q_group = False
            a_group = False
            o_group = False
            role_name =""  
            if form.is_valid():
                myGoalStatus = GoalStatus.objects.get(status_name = form.cleaned_data['goal_status'])
                weeklyGoalStatus = GoalStatus.objects.get(status_name = 'Weekly Goal')
                dailyGoalStatus = GoalStatus.objects.get(status_name = 'Daily Goal')
                verifyGoalStatus = GoalStatus.objects.get(status_name = 'Verify Goal')
                doneGoalStatus = GoalStatus.objects.get(status_name = 'Done Goal')

                forms = form.cleaned_data['goal_status']

                if (request.user.groups.filter(name ='Quality Assurance').first() is not None):
                    q_group = True
                    role_name ="Quality Assurance"            

                if (request.user.groups.filter(name ='Developer').first() is not None):
                    d_group = True
                    role_name ="Developer"

                if (request.user.groups.filter(name ='Admin').first() is not None):
                    a_group = True

                if (request.user.groups.filter(name ='Owner').first() is not None):
                    o_group = True

                if obj.goal_status == forms:
                    return HttpResponse("you cannot move your current goal back to to the same status")

                if d_group:
                    if forms == doneGoalStatus:
                        return HttpResponse("A user with '"+role_name+"'group cannot move goal to done")
                    if obj.user != request.user :
                        return HttpResponse("A user with '"+role_name+"' can only move self created goals")

                if q_group:
                    # if obj.user == request.user :
                    #     if forms == weeklyGoalStatus:
                    #         return HttpResponse("A user with '"+role_name+"' group can not move self created goal to weekly goal")
                    if obj.user != request.user :
                        if forms != doneGoalStatus:
                            return HttpResponse("A user with '"+role_name+"' group can only move other user goal to done goal")

                obj.goal_status = forms
                obj.save()
                return HttpResponse("Goal was moved successfully")
                # return redirect("dotman007_richardscrumy:home")

                
        else:
            form = MoveGoalForm()
            return render(request, 'dotman007_richardscrumy/move_goal.html', context={"move": form})



@login_required(login_url='dotman007_richardscrumy:login_page')
def add_goal(request):
    if request.method == 'POST':
        goal = CreateGoalForm(request.POST)
        d_group = False
        q_group = False
        a_group = False
        o_group = False
        role_name =""        

        if goal.is_valid():
            myGoalStatus = GoalStatus.objects.get(status_name = 'Weekly Goal')
            weeklyGoalStatus = GoalStatus.objects.get(status_name = 'Weekly Goal')
            myGoalUser = User.objects.get(username = request.user.username)
                       
            if (request.user.groups.filter(name ='Quality Assurance').first() is not None):
                q_group = True
                role_name ="Quality Assurance"            

            if (request.user.groups.filter(name ='Developer').first() is not None):
                d_group = True
                role_name ="Developer"

            if (request.user.groups.filter(name ='Admin').first() is not None):
                a_group = True

            if (request.user.groups.filter(name ='Owner').first() is not None):
                o_group = True
           
            if (((d_group or q_group ) and myGoalStatus == weeklyGoalStatus and request.user.username == myGoalUser.username) or (a_group or o_group)):
                ScrumyGoals.objects.create(
                    user=User.objects.get(username=request.user.username),
                    goal_name=goal.cleaned_data['goal_name'],
                    goal_id= random.randint(1000,9999),
                    created_by = 'Louis',
                    moved_by='Louis',
                    owner='Louis',
                    goal_status=GoalStatus.objects.get(status_name='Weekly Goal') 
                )
            elif ((d_group or q_group ) and myGoalStatus != weeklyGoalStatus and request.user.username == myGoalUser.username):               
                return HttpResponse("A user with '"+role_name+"' group can only create weekly goal")           

            elif ((d_group or q_group ) and request.user.username != myGoalUser.username):
                return HttpResponse("A user with '"+role_name+"' group can only create goal for self")
            
            return HttpResponse("Goal Added successfully")
            
        else:
            return redirect("dotman007_richardscrumy:add_goal")
    
    else:
        goal = CreateGoalForm()
        return render(request, 'dotman007_richardscrumy/add_goal.html', context={"goal": goal})


@login_required(login_url='dotman007_richardscrumy:login_page')
def home(request):
    users = User.objects.all()
    daily_set=GoalStatus.objects.get(status_name='Daily Goal').scrumygoals_set.all()
    weekly_set=GoalStatus.objects.get(status_name='Weekly Goal').scrumygoals_set.all()
    verify_set=GoalStatus.objects.get(status_name='Verify Goal').scrumygoals_set.all()
    done_set=GoalStatus.objects.get(status_name='Done Goal').scrumygoals_set.all()
    d_group= False
    goal = CreateGoalForm()
    

    if (request.user.groups.filter(name ='Developer').first() is not None):
        d_group = True

    form = MoveGoalForm()  
    
    context= {'users':users, 'daily' : daily_set, 'weekly': weekly_set, 'verify': verify_set, 'done': done_set ,'d_group':d_group,'move':form,'groupList':Group.objects.all(), 'goal':goal}

    return render(request, 'dotman007_richardscrumy/home.html', context)

@login_required(login_url='dotman007_richardscrumy:login_page')
def change_group(request):
    if request.method == 'POST':
        groups = GroupChange(request.POST)        
        if groups.is_valid():
            my_group= Group.objects.get(name=groups.cleaned_data['group_name'])
            user = User.objects.get(username=request.user.username)
            user.groups.clear()
            my_group.user_set.add(user)
            my_group.save()
            return HttpResponse('role changed successfully')
        else:
            groups = GroupChange()
            context={
                'groupList':Group.objects.all(),
                'group':groups
            }            
            return render(request, 'dotman007_richardscrumy/group_name.html', context)
    else:
        groups = GroupChange()
        context={
                'groupList':Group.objects.all(),
                'group':groups
            }       
        
        return render(request, 'dotman007_richardscrumy/group_name.html',context )