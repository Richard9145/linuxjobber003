from django.apps import AppConfig


class Dotman007RichardscrumyConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'dotman007_richardscrumy'
