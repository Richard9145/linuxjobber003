from django.db import models
from django.contrib.auth.models import User
from django.db.models import fields
from django.forms import ModelForm



# Create your models here.

class GoalStatus(models.Model):
    status_name=models.CharField(max_length=300)

    def __str__(self):
        return self.status_name


class ScrumyGoals(models.Model):
    goal_name=models.CharField(max_length=100)
    goal_id = models.IntegerField()
    created_by = models.CharField(max_length=100)
    moved_by = models.CharField(max_length=100)
    owner=models.CharField(max_length=50)
    goal_status=models.ForeignKey(GoalStatus, on_delete=models.PROTECT)
    user=models.ForeignKey(User, on_delete=models.CASCADE, related_name='louis')

    def __str__(self):
        return self.goal_name

    


class ScrumyHistory(models.Model):
    moved_by=models.CharField(max_length=100)
    created_by=models.CharField(max_length=50)
    moved_from=models.CharField(max_length=50)
    moved_to=models.CharField(max_length=50)
    time_of_action=models.DateTimeField()
    goal=models.ForeignKey(ScrumyGoals, on_delete=models.PROTECT)

    def __str__(self):
        return self.created_by
    
class SignupForm(ModelForm):
    class Meta:
        model= User
        fields =['first_name', 'last_name', 'email', 'username', 'password']


class CreateGoalForm(ModelForm):
    class Meta:
        model= ScrumyGoals
        fields= ['goal_name', 'user',]

