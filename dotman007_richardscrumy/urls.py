from django.urls import path, include
from . import views

app_name = "dotman007_richardscrumy"

urlpatterns = [
    # Lab 21
    path('register/', views.signUp, name='register'),
    path('login_page/', views.loginPage, name='login_page'),
    path('logout_page/', views.logoutUser, name='logout_page'),
    path('group/', views.change_group, name='group'),

    
    path('', views.index, name="about"),
    path('goal/', views.goal, name='goal'),
    path('success/', views.success, name='success'),
    path('move_goal/<int:goal_id>/', views.move_goal, name="move_goal"),

    # url path lab 13
    path('addgoal/', views.add_goal, name="add_goal"),

    path('home/', views.home, name="home"),

    # lab 17
    path('accounts/', include('django.contrib.auth.urls'))
]
