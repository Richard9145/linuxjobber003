// Call the dataTables jQuery plugin
$(document).ready(function () {
  $('#dataTable').DataTable();

  $(".moveGoal").click(function () {
    goalId = $(this).attr("goalid");
    $("#moveGoalModalForm").attr("action", "/dotman007_richardscrumy/move_goal/" + goalId + "/")
    // alert($(this).attr("goalid"))
  })


  $("#moveGoalModalSubmit").click(function () {
    var datastring = $("#moveGoalModalForm").serialize();   
    $.ajax({
      type: "POST",
      url: $("#moveGoalModalForm").attr("action"),
      data: datastring,
      dataType: "json",
      success: function (data) {
        console.log(data)
       
      },
      error: function (e) {
        console.log(e)
        alert(e.responseText);
        if (e.responseText.indexOf("successfully") >= 0) {
          location.reload(true);
        }
      }
    });
  })



  $("#changeGroupSubmit").click(function () {
    var datastring = $("#changeGroupForm").serialize();
   
    $.ajax({
      type: "POST",
      url: $("#changeGroupForm").attr("action"),
      data: datastring,
      dataType: "json",
      success: function (data) {
        console.log(data)       
      },
      error: function (e) {
        console.log(e)
        alert(e.responseText);
        if (e.responseText.indexOf("successfully") >= 0) {
          location.reload(true);
        }
      }
    });
  })

  $("#addGoalSubmit").click(function () {
    var datastring = $("#addGoalForm").serialize();
   
    $.ajax({
      type: "POST",
      url: $("#addGoalForm").attr("action"),
      data: datastring,
      dataType: "json",
      success: function (data) {
        console.log(data)       
      },
      error: function (e) {
        console.log(e)
        alert(e.responseText);
        if (e.responseText.indexOf("successfully") >= 0) {
          location.reload(true);
        }
      }
    });
  })

});
