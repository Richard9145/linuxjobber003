import os
from setuptools import find_packages, setup

with open (os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# allow setup.py to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name = "django-dotman007_richardscrumy",
    version = "0.0.4",
    packages= find_packages(),
    include_package_data=True,
    license = "BSD License",
    author = "Adedotun Adelaja",
    author_email = "dotun.smeep@gmail.com",
    description = ("An demonstration of how to create, document, and publish "
                                   "to the cheese shop a5 pypi.org."),
    long_description = README,
    url = "http://packages.python.org/an_example_pypi_project",
    classifiers=[
        "Environment :: Web Environment",
        "Framework :: Django",
        "Framework :: Django :: 3.2",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "programming Language :: Python",
        "programming Language :: Python :: 3.5",
        "programming Language :: Python :: 3.6",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: Dynamic Content",
        
    ],
)