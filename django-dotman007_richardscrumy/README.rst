============
scrumyproject
=============

scrumyproject is a simple django app that help resolve doctors appointment 
in various hospitals. With this app, patients can book for doctors appointment 
and as well get their results without having to be physically present in the clinic.